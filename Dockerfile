# Définition de l’image de base
FROM php:7.1
# Mise à jour des paquets
RUN apt-get update
# Installation de PHP et des dépendances Composer
RUN apt-get install -qq git curl libmcrypt-dev libjpeg-dev libpng-dev libfreetype6-dev libbz2-dev
# Nettoyage du dépôt local
RUN apt-get clean
# Installation des paquets supplémentaires
# Vous pouvez ajouter ici toutes les ressources nécessaires à la validation de votre projet
RUN docker-php-ext-install mcrypt pdo_mysql zip
# Installation de Composer
RUN curl --silent --show-error https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
# Installation de Laravel Envoy
RUN composer global require "laravel/envoy=~1.0"

